﻿using Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;

namespace Contracts.Interfaces.User
{
    [ServiceContract]
    public interface IGroupService
    {
        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        GroupList GetGroupList();
    }
}
