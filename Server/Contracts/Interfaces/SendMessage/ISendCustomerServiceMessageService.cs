﻿using Models.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;

namespace Contracts.Interfaces.SendMessage
{
    [ServiceContract]
    public interface ISendCustomerServiceMessageService
    {
        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        void SendCustomerServiceTextMessage(CustomerServiceTextMessage customerServiceTextMessage);
    }
}
