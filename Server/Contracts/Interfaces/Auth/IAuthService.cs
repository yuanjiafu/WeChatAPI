﻿using Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;

namespace Contracts.Interfaces.Auth
{
    [ServiceContract]
    public interface IAuthService
    {
        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        void Auth(AuthInfo authInfo);

        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        string GetOpenIDByOAuthCode(string code);
    }
}
