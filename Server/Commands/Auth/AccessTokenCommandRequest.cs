﻿using CommonLib.CacheOperation;
using CommonLib.NetOperation;
using Models.Base;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.Auth
{
    /// <summary>
    /// 微信交互接口凭证命令请求
    /// </summary>
    public class AccessTokenCommandRequest : BaseCommandRequest
    {
        public AccessTokenCommandRequest()
        {
            this.Method = HttpMethod.GET;
            this.URL = string.Format("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}",
                AppConfigCache.GetAppSettings(CacheKeySet.WeChatAPPID.ToString()),
                AppConfigCache.GetAppSettings(CacheKeySet.WeChatAPPSecret.ToString()));
        }
    }
}
