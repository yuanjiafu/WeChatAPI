﻿using CommonLib.CacheOperation;
using CommonLib.NetOperation;
using Models.Auth;
using Models.Base;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.Auth
{
    /// <summary>
    /// 获取OAuth2.0授权访问信息命令请求
    /// </summary>
    public class GetOAuthAccessInfoCommandRequest : BaseCommandRequest
    {
        public GetOAuthAccessInfoCommandRequest(OAuthCondition oauthCondition)
        {
            this.Method = HttpMethod.GET;
            this.URL = string.Format("https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type={3}",
                AppConfigCache.GetAppSettings(CacheKeySet.WeChatAPPID.ToString()),
                AppConfigCache.GetAppSettings(CacheKeySet.WeChatAPPSecret.ToString()),
                oauthCondition.Code,
                oauthCondition.GrantType);
        }
    }
}
