﻿using DispatchingCenter.Dispatch;
using Events.SendMessage;
using Models.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Businesses.Menu
{
    public class MenuOperation
    {
        public void CreateMenu(MenuList menuList)
        {
            var createMenuEvent = new CreateMenuEvent() { MenuList = menuList };

            Dispatcher.ActiveEvent(createMenuEvent);
        }

        public MenuList GetMenu()
        {
            var getMenuEvent = new GetMenuEvent();

            Dispatcher.ActiveEvent(getMenuEvent);

            return getMenuEvent.MenuList;
        }
    }
}
