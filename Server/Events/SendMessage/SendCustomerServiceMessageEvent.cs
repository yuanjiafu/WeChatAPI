﻿using DispatchingCenter.Base;
using Models.Base;
using Models.Interface;
using Models.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.SendMessage
{
    /// <summary>
    /// 发送客服消息事件
    /// </summary>
    public class SendCustomerServiceMessageEvent : DispatchEvent, IAccessTokenAuth
    {
        public string AccessToken { get; set; }

        public BaseCustomerServiceMessage CustomerServiceMessage { get; set; }
    }
}
