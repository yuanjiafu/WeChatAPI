﻿using DispatchingCenter.Base;
using Models.Interface;
using Models.Menu;
using Models.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.SendMessage
{
    /// <summary>
    /// 查询菜单事件
    /// </summary>
    public class GetMenuEvent : DispatchEvent, IAccessTokenAuth
    {
        public string AccessToken { get; set; }

        public MenuList MenuList { get; set; }
    }
}
