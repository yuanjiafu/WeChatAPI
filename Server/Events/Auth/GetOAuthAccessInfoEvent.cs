﻿using DispatchingCenter.Base;
using Models.Auth;
using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.Auth
{
    /// <summary>
    /// 获取OAuth2.0授权访问信息事件
    /// </summary>
    public class GetOAuthAccessInfoEvent : DispatchEvent
    {
        /// <summary>
        /// OAuth2.0条件
        /// </summary>
        public OAuthCondition OAuthCondition { get; set; }

        /// <summary>
        /// Auth2.0授权访问信息
        /// </summary>
        public OAuthAccessInfo OAuthAccessInfo { get; set; }
    }
}
