﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Data
{
    /// <summary>
    /// 缓存主键
    /// </summary>
    public enum CacheKeySet
    {
        #region AppConfig

        /// <summary>
        /// 微信第三方用户唯一凭证
        /// </summary>
        WeChatAPPID,

        /// <summary>
        /// 微信第三方用户唯一凭证密钥
        /// </summary>
        WeChatAPPSecret,

        #endregion

        #region Auth

        /// <summary>
        /// 微信交互接口凭证
        /// </summary>
        AccessToken,

        #endregion
    }
}
