﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Menu
{
    /// <summary>
    /// 跳转URL菜单
    /// </summary>
    [DataContract(Name = "view_menu")]
    public class ViewMenu : BaseMenu
    {
        /// <summary>
        /// 类型
        /// </summary>
        [DataMember(Name = "type")]
        public string Type { get { return "view"; } set { } }

        /// <summary>
        /// 跳转的URL
        /// </summary>
        [DataMember(Name = "url")]
        public string Url { get; set; }
    }
}
