﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Menu
{
    /// <summary>
    /// 菜单列表
    /// </summary>
    [DataContract(Name = "menu_list")]
    public class MenuList
    {
        /// <summary>
        /// 菜单集合
        /// </summary>
        [DataMember(Name = "button")]
        public List<BaseMenu> MenuSet { get; set; }
    }
}
