﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Base
{
    /// <summary>
    /// 文本消息基类
    /// </summary>
    [DataContract(Name = "text")]
    public class BaseTextMessage
    {
        /// <summary>
        /// 文本内容
        /// </summary>
        [DataMember(Name = "content")]
        public string Content { get; set; }
    }
}
