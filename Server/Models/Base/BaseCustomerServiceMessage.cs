﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Base
{
    /// <summary>
    /// 客服消息基类
    /// </summary>
    [DataContract]
    public class BaseCustomerServiceMessage
    {
        /// <summary>
        /// 普通用户openid
        /// </summary>
        [DataMember(Name = "touser")]
        public string ToUser { get; set; }

        /// <summary>
        /// 消息类型，text
        /// </summary>
        [DataMember(Name = "msgtype")]
        public string MessageType { get; set; }
    }
}
