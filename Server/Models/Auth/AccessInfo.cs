﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Auth
{
    /// <summary>
    /// 微信交互接口凭证信息
    /// </summary>
    [DataContract(Name = "access_token_info")]
    public class AccessTokenInfo
    {
        /// <summary>
        /// 获取到的凭证
        /// </summary>
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 凭证有效时间，单位：秒
        /// </summary>
        [DataMember(Name = "expires_in")]
        public int ExpiresIn { get; set; }
    }
}
