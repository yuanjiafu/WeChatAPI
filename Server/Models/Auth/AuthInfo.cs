﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Auth
{
    [DataContract]
    public class AuthInfo
    {
        [DataMember]
        public string Signature { get; set; }

        [DataMember]
        public string Timestamp { get; set; }

        [DataMember]
        public string Nonce { get; set; }

        [DataMember]
        public string Echostr { get; set; }
    }
}
