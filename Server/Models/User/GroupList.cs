﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.User
{
    /// <summary>
    /// 公众平台分组信息列表
    /// </summary>
    [DataContract(Name = "group_list")]
    public class GroupList
    {
        /// <summary>
        /// 公众平台分组信息列表
        /// </summary>
        [DataMember(Name = "groups")]
        public List<GroupInfo> GroupInfoList { get; set; }
    }
}
