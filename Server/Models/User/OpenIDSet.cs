﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.User
{
    /// <summary>
    /// OpenID集合
    /// </summary>
    [DataContract(Name = "openid_set")]
    public class OpenIDSet
    {
        /// <summary>
        /// OpenID列表
        /// </summary>
        [DataMember(Name = "openid")]
        public List<string> OpenIDList { get; set; }
    }
}
