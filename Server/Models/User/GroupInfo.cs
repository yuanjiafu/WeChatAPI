﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.User
{
    /// <summary>
    /// 公众平台分组信息
    /// </summary>
    [DataContract(Name = "group")]
    public class GroupInfo
    {
        /// <summary>
        /// 分组id，由微信分配
        /// </summary>
        [DataMember(Name = "id")]
        public int ID { get; set; }

        /// <summary>
        /// 分组名字，UTF8编码
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// 分组内用户数量
        /// </summary>
        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}
