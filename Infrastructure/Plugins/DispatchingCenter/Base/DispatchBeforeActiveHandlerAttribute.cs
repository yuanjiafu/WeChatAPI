﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DispatchingCenter.Base
{
    /// <summary>
    /// 调度激活前事件的处理器
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class DispatchBeforeActiveHandlerAttribute : DispatchHandlerAttribute
    {
        public DispatchBeforeActiveHandlerAttribute()
            : base(typeof(DispatchHandlerAttribute))
        {

        }
    }
}
