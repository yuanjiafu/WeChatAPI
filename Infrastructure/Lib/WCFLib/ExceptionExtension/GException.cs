﻿using CommonLib.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using WCFLib.ExceptionExtension.Enums;

namespace WCFLib.ExceptionExtension
{
    public class GException : Exception
    {
        private string _code;
        private string _message;

        /// <summary>
        /// 异常消息
        /// </summary>
        public new string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// 错误代码
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// 异常类型
        /// </summary>
        public ExceptionType ExceptionType { get; set; }

        private GException()
            : base()
        { }

        public GException(Enum exceptionEnum)
            : base(EnumHelper.GetEnumDescription(exceptionEnum).Description)
        {
            _message = EnumHelper.GetEnumDescription(exceptionEnum).Description;
            this.ErrorCode = (int)Enum.Parse(exceptionEnum.GetType(), Enum.GetName(exceptionEnum.GetType(), exceptionEnum));
            this.ExceptionType = Enums.ExceptionType.ErrorCode;
        }

        public GException(string message, ExceptionType exceptionType)
            : base(message)
        {
            _message = message;
            this.ExceptionType = exceptionType;
        }

        public GException(string message, Exception innerException, ExceptionType exceptionType)
            : base(message, innerException)
        {
            _message = message;
            this.ExceptionType = exceptionType;
        }
    }
}
