﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFLib.ExceptionExtension.Enums
{
    public enum ExceptionType
    {
        /// <summary>
        /// 未知异常
        /// </summary>
        Unknow = 0,
        /// <summary>
        /// 中断异常
        /// </summary>
        Interrupted = 1,
        /// <summary>
        /// 异常代码
        /// </summary>
        ErrorCode = 2,
    }
}
