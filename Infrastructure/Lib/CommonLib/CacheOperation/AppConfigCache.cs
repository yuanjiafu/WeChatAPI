﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace CommonLib.CacheOperation
{
    /// <summary>
    /// 应用配置缓存
    /// </summary>
    public static class AppConfigCache
    {
        #region GetAppSettings

        private static ConcurrentDictionary<string, string> _dicConfigAppSettings = new ConcurrentDictionary<string, string>();
        private static Dictionary<string, Lazy<string>> _dicLazyValue = new Dictionary<string, Lazy<string>>();

        private static object _locker = new object();

        public static string GetAppSettings(string key)
        {
            if (!_dicLazyValue.ContainsKey(key))
            {
                lock (_locker)
                {
                    if (!_dicLazyValue.ContainsKey(key))
                    {
                        _dicLazyValue.Add(key, new Lazy<string>(() =>
                        {
                            return ConfigurationManager.AppSettings[key];
                        }));
                    }
                }
            }

            return _dicConfigAppSettings.GetOrAdd(key, _dicLazyValue[key].Value);
        }

        #endregion
    }
}
